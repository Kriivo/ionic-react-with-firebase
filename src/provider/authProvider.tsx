import { createContext, useState } from "react";
import { getCurrentUser, onCreateNewUser, onLogin, onLogout } from "../config/firebaseConfig";
import { User } from "../model/models";

type AuthContextType = {
    user: boolean;
    setUser: React.Dispatch<React.SetStateAction<boolean>>
    singup: (props: User) => void;
    login: (props: User) => void;
    logout: () => void;
    currentUser: () => void;
}

type AuthContextProviderProps = {
    children: React.ReactNode
}

export const AuthContext = createContext<AuthContextType | null>( {} as AuthContextType);

const AuthProvider = ({ children }: AuthContextProviderProps) => {
    const [user, setUser] = useState(false);

    /**
     * Metodo para registrar nuevo usuario
     * @param User Se debe pasar una interface de tipo User
     */
    const singup = async (props: User) => {
        console.log("Metodo singup!! " + props);
        onCreateNewUser(props.email, props.password).then((res) => {
            if (!res) {
                setUser(res)
            }
        });
    }

    /**
     * Metodo para iniciar sesion
     * @param props 
     */
    const login = async (props: User) => {
        console.log("Metodo singin!!");
        await onLogin(props.email, props.password).then((res) => {
            if (res) {
                setUser(true)
            }
        });
    }

    /**
     * Metodo para cerrar sesion
     * @param props 
     */
    const logout = () => {
        console.log("Metodo logout!!");
        onLogout()
        setUser(false)
    }

    const currentUser = async() => {
        console.log("Current User");
        await getCurrentUser().then((res) =>{
            if (res) {
                setUser(true)
            } else {
                setUser(false)
            }
        });
    }

    return (
        <AuthContext.Provider value={{ user: user, setUser, singup, login, logout, currentUser }}>
            {children}
        </AuthContext.Provider>
    );
}

export { AuthProvider }