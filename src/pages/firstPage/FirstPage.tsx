import { IonButton, IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useContext } from 'react';
import Example from '../../components/example/Example';
import { AuthContext } from '../../provider/authProvider';

interface ContainerProps {
    name: string;
    date: string;
}

const FirstPage: React.FC<ContainerProps> = ({ name, date }) => {
    const authContext = useContext(AuthContext);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>{name}</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{name}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <Example name="Juan carlos" body="asdasd as da sd as dasd asd" date="12/12/12" />
            </IonContent>
            <IonButton onClick={() => {
                authContext?.logout();
            }}>
                Cerrar session
            </IonButton>
        </IonPage>
    );
};

export default FirstPage;