import { IonButton, IonContent, IonHeader, IonInput, IonItem, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useContext, useEffect } from 'react';
import { useForm, SubmitHandler } from "react-hook-form";
import { useHistory } from 'react-router';
import { IFormInput, User } from '../../model/models';
import { AuthContext } from '../../provider/authProvider';


const Register: React.FC = () => {
    const authContext = useContext(AuthContext);
    const history = useHistory()
    const { register, formState: { errors }, handleSubmit } = useForm<IFormInput>();

    const onSubmit: SubmitHandler<IFormInput> = (data:User) => {
        if (authContext) {
            authContext.singup(data);
        }
    };

    useEffect(() =>{
        if (!authContext?.user) {
            history.push("/login")
        }
    })

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Register</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <form onSubmit={handleSubmit(onSubmit)}>

                    <IonItem lines="full">
                        <IonInput
                            {...register("email", { required: true })}
                            placeholder="Email"
                            type="email"
                        />
                        <p>
                            {errors.email?.type === 'required' && "Email es requerido."}
                        </p>
                    </IonItem>

                    <IonItem lines="full">
                        <IonInput
                            {...register("password", { required: true })}
                            placeholder="Password"
                            type="password"
                            minlength={6}
                        />
                        <p>
                            {errors.password?.type === 'required' && "Password es requerido."}
                        </p>
                    </IonItem>

                    <IonButton
                        color="secondary"
                        type="submit"
                    >
                        Registrar
                    </IonButton>
                </form>
            </IonContent>
        </IonPage>
    );
};

export default Register;