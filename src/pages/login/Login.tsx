import { IonButton, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useContext, useEffect } from 'react';
import { useForm, SubmitHandler } from "react-hook-form";
import { useHistory } from 'react-router';
import { User } from '../../model/models';
import { AuthContext } from '../../provider/authProvider';

interface IFormInput {
    email: string;
    password: string;
}

const Login: React.FC = () => {
    const authContext = useContext(AuthContext);
    const history = useHistory()
    const { register, formState: { errors }, handleSubmit } = useForm<IFormInput>();
    const onSubmit: SubmitHandler<IFormInput> = (data:User) => {
        if (authContext) {
            authContext.login(data);
        }
    };

    useEffect(() =>{
        if (authContext?.user) {
            history.push("/page/FirstPage")
        }
    })

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Login</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <form onSubmit={handleSubmit(onSubmit)}>

                    <IonItem lines="full">
                        <IonLabel position="floating">Email</IonLabel>
                        <IonInput
                            {...register("email", { required: true })}
                            type="email"
                        />
                    </IonItem>
                    <p>
                        {errors.email?.type === 'required' && "Email es requerido."}
                    </p>

                    <IonItem lines="full">
                        <IonLabel position="floating">Password</IonLabel>
                        <IonInput
                            {...register("password", { required: true })}
                            type="password"
                            minlength={6}
                        />
                    </IonItem>
                    <p>
                        {errors.password?.type === 'required' && "Password es requerido."}
                    </p>

                    <IonButton
                        color="secondary"
                        type="submit"
                    >
                        Login
                    </IonButton>
                </form>
            </IonContent>
        </IonPage>
    );
};

export default Login;