import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAQNpW_AEVcDFbEpnsgjP_6GdtXpS3IyI4",
    authDomain: "gym-app-461e0.firebaseapp.com",
    projectId: "gym-app-461e0",
    storageBucket: "gym-app-461e0.appspot.com",
    messagingSenderId: "510315954816",
    appId: "1:510315954816:web:ad165c98a3904fd6339c65",
    measurementId: "G-ETD5LTWFXB"
};

firebase.initializeApp(firebaseConfig);

export function getCurrentUser() {
    console.log("current sin provider");
    
    return new Promise<string | null | undefined>((resolve, reject) => {
        firebase.auth().onAuthStateChanged(function (res) {
            const user = res?.email;
            if (user) {
                resolve(user)
            } else {
                resolve(null)
            }
            // unsubscribe()
        })
    })
}



export function onCreateNewUser(email: string, password: string) {
    return new Promise<boolean>((resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((res) => {
                // Signed in
                alert('Se registro con exito !!');
                resolve(false)
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/weak-password') {
                    alert('The password is too weak.');
                } else {
                    if (errorCode === "auth/email-already-in-use") {
                        console.log("Este email se encuentra en uso por otra cuenta.");

                    }
                    alert(errorMessage);
                }
                // console.log(error);
                resolve(error)
            });
    })
}

export function onLogin(email: string, password: string) {
    return new Promise((resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((res) => {
                resolve(true)
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/wrong-password') {
                    alert('Wrong password.');
                } else {
                    alert(errorMessage);
                    reject(false)
                }
                console.log(error);
            });
    })
}

export async function onLogout() {
    return await new Promise(async (resolve, reject) => {
        await firebase.auth().signOut();
    })
}