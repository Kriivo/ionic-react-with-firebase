
interface ContainerProps {
    name: string;
    body: string;
    date: string;
}

const Example: React.FC<ContainerProps> = ({ name, body, date }) => {

    return (
        <div>
            <h4>Holassss {name}</h4>
            <p>{body}</p>
            <div> - {date} - </div>
        </div>
    );
};

export default Example;