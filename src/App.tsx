import { useEffect, useState } from 'react';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';
import Menu from './components/menu/Menu';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import FirstPage from './pages/firstPage/FirstPage';
import { getCurrentUser } from './config/firebaseConfig';
import Login from './pages/login/Login';
import Register from './pages/register/Register';

const RoutingSystem: React.FC = () => {
  return (
    <IonReactRouter>
      <IonSplitPane contentId="main">
        <Menu />
        <IonRouterOutlet id="main">
          <Route path="/" exact={true}>
            <Redirect to="/register" />
          </Route>
          <Route path="/page/FirstPage" exact={true}>
            <FirstPage name="Juan carlox" date="12.12.12" />
          </Route>
          <Route path="/login" exact={true}>
            <Login />
          </Route>
          <Route path="/register" exact={true}>
            <Register />
          </Route>
        </IonRouterOutlet>
      </IonSplitPane>
    </IonReactRouter>
  );
}

const App: React.FC = () => {

  const [busy, setBusy] = useState(true)

  useEffect(() => {
    getCurrentUser().then(user => {
      if (user) {
        <Redirect exact to="/home" />
      } else {
        <Redirect exact to="/login" />
      }
      setBusy(false)
    })
  }, [])

  return (
    <IonApp>
      { busy ? 'spinner here' : <RoutingSystem />}
    </IonApp>
  );
};

export default App;
