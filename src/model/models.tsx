export interface User {
    email: string;
    password: string;
}

export interface IFormInput {
    email: string;
    password: string;
}

export interface AuthUser {
    isAuth: boolean;
}